import {countries} from './countries';

export const formConfig = [
  {type:'text', label:'First name', key:'firstName', validators:['required'], options:[], defaultPlaceHolder:''},
  {type:'text', label:'Last name', key:'lastName', validators:['required'], options:[], defaultPlaceHolder:''},
  {type:'radio', label: 'Gender', key:'gender', validators:['required'], options:[
      {label: 'Male', value:'male'},
      {label: 'Female', value:'female'},
      {label: 'Other', value:'other'}
    ], defaultPlaceHolder:''},
  {type:'select', label:'Countries', key: 'country', validators:['required'], defaultPlaceHolder:'Choose placeholder...', options: countries},
  {type:'text', label:'City', key:'city', validators:['required'], options:[], defaultPlaceHolder:''},
  {type:'text', label:'Street', key:'street', validators:['required'], options:[], defaultPlaceHolder:''},
  {type:'number', label:'Street number', key:'streetNumber', validators:['required'], options:[], defaultPlaceHolder:''},
  {type:'text', label:'Username', key:'username', validators:['required'], options:[], defaultPlaceHolder:''},
  {type:'password', label:'Password', key:'password', validators:['required'], options:[], defaultPlaceHolder:''}
];
