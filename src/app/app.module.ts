import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { TestFormComponent } from './components/test-form/test-form.component';
import { DynamicFormComponent } from './components/dynamic-form/dynamic-form.component';
import { RecursiveComponent } from './components/recursive/recursive.component';
import { ChangeColorDirective } from './directives/changeColorOnHover';
import { ParentComponent } from './components/parent/parent.component';
import { ChildComponent } from './components/child/child.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'signup', component: SignUpComponent},
  {path: 'test', component: TestFormComponent},
  {path: 'dynamic', component: DynamicFormComponent},
  {path: 'recursive', component: RecursiveComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignUpComponent,
    TestFormComponent,
    DynamicFormComponent,
    RecursiveComponent,
    ChangeColorDirective,
    ParentComponent,
    ChildComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
