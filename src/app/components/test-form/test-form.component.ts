import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-test-form',
  templateUrl: './test-form.component.html'
})
export class TestFormComponent implements OnInit {
  
  testForm: FormGroup;

  constructor() { 
    this.testForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      address: new FormGroup({
        street: new FormControl('', [Validators.required, Validators.minLength(3)])
      })
    });
  }

  ngOnInit() {
  }

}
