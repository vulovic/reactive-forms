import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html'
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  submitTried: boolean = false;

  modalIsActive = false;


  constructor() {
    this.signUpForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      gender: new FormControl('',  Validators.required),
      address: new FormGroup({
        country: new FormControl('', Validators.required),
        city: new FormControl('', Validators.required),
        zip: new FormControl('', Validators.required),
        street: new FormControl('', Validators.required),
        streetNumber: new FormControl('', Validators.required)
      }),
      email: new FormControl('',  [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      password: new FormControl('',  [Validators.required, Validators.minLength(8)]),
      rules: new FormControl(false, Validators.requiredTrue)
    })
  }

  ngOnInit() {
  }

  toggleModal(){
    this.modalIsActive = !this.modalIsActive;
  }

  submit(){
    this.submitTried = true;
    if(this.signUpForm.valid){
      console.log("OK");
      this.signUpForm.value;
    }
    else {
      console.log("Noooo")
    }
  }

}
