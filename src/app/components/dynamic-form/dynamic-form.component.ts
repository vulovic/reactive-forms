import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import {countries} from '../../configs/countries';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html'
})
export class DynamicFormComponent implements OnInit {



  formConfig = [
    {type:'text', label:'First name', key:'firstName', validators:['required'], fields: [], options:[], defaultPlaceHolder:''},
    {type:'text', label:'Last name', key:'lastName', validators:['required'], fields: [], options:[], defaultPlaceHolder:''},
    {type:'radio', label: 'Gender', key:'gender', validators:['required'], options:[
        {label: 'Male', value:'male'},
        {label: 'Female', value:'female'},
        {label: 'Other', value:'other'}
    ], fields: [], defaultPlaceHolder:''},
    {type:'select', label:'Countries', key: 'country', validators:['required'], defaultPlaceHolder:'Choose placeholde', options: countries, fields: []},
    {type:'subgroup', label:'Address', validators:[], key: 'address', fields:[
        {type:'text', label:'City', key:'city', validators:['required']},
        {type:'text', label:'Street', key:'street', validators:['required']},
        {type:'number', label:'Street number', key:'streetNumber', validators:['required']}
      ], options:[], defaultPlaceHolder:''},
    {type:'text', label:'Username', key:'username', validators:['required'], fields: [], options:[], defaultPlaceHolder:''},
    {type:'password', label:'Password', key:'password', validators:['required'], fields: [], options:[], defaultPlaceHolder:''}

  ];
  testForm: FormGroup;
  submitTried = false;
  countries = [];

  constructor() {
    this.countries = countries;
    this.testForm = new FormGroup(this.makeForm());
  }

  makeForm(){
    let testFormObject =  {};
    let arrayValidators = [];

    for (let i = 0; i < this.formConfig.length; i++){

      if(this.formConfig[i].type!='subgroup'){
        arrayValidators = [];
        for (let j = 0; j < this.formConfig[i].validators.length; j++){
          arrayValidators.push(Validators[this.formConfig[i].validators[j]]);
        }
        testFormObject[this.formConfig[i].key]= new FormControl('', arrayValidators);
      } else {
        let subgroupObject = {};
        for(let k = 0; k < this.formConfig[i].fields.length; k++){
          arrayValidators = [];
          for (let j = 0; j < this.formConfig[i].fields[k].validators.length; j++){
            arrayValidators.push(Validators[this.formConfig[i].fields[k].validators[j]]);
          }
          subgroupObject[this.formConfig[i].fields[k].key] = new FormControl('', arrayValidators);
        }
        testFormObject[this.formConfig[i].key]= new FormGroup(subgroupObject);
      }
    }
    return testFormObject;
  }

  ngOnInit() {
  }

  submit(){
    this.submitTried = true;
    if(this.testForm.status!=="INVALID"){
      console.log(this.testForm.value);
    } else {
      console.log("INVALID");
      console.log(this.testForm.value);
    }
  }

}
