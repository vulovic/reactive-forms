import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup, FormArray} from '@angular/forms';
import {formConfig} from '../../configs/formConfig';

@Component({
  selector: 'app-recursive',
  templateUrl: './recursive.component.html'
})
export class RecursiveComponent{



  testForm: FormGroup;
  submitTried = false;

  formConfig = formConfig;

  constructor() {
    this.testForm = new FormGroup(this.processData(this.formConfig));
  }

  processData(data){
      if(!data) return;
      let testFormObject = {};
      let arrayValidators = [];
      for(let i = 0; i < data.length; i++){
        arrayValidators = [];
        for (let j = 0; j < data[i].validators.length; j++){
          arrayValidators.push(Validators[data[i].validators[j]]);
        }
        testFormObject[data[i].key]= new FormControl('', arrayValidators);
        if(data[i].type=='subgroup'){
            this.processData(data[i].fields);
        }
      }
      return testFormObject;
  }

  submit(){
    this.submitTried = true;
    if(this.testForm.status!=="INVALID"){
      let {country, city, street, streetNumber} = this.testForm.value;
      let address = {country, city, street, streetNumber};
      console.log(address);
    } else {
      console.log("INVALID");
      console.log(this.testForm.value);
    }
  }

}

