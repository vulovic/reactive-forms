import { Component, OnInit } from '@angular/core';

import {ChildComponent} from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html'
})
export class ParentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  consoleMe(){
    console.log('nadl;lda;');
  }

  consoleMeAgain(str){
    console.log(str);
  }

}
