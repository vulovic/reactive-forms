import { Component, OnInit, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html'
})
export class ChildComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Output('ifICall') ifICall = new EventEmitter<any>();
  @Output('ifICallAgain') ifICallAgain = new EventEmitter<any>();

  sendEventToParent(){
    this.ifICall.emit(true);
  }
  sendEventToParentAgain(str){
    this.ifICallAgain.emit(str);
  }

}
