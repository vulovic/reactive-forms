import {Directive, ElementRef, Input} from "@angular/core";

@Directive({
    selector: "[changeColor]" /**attribute */
})

export class ChangeColorDirective{

    @Input('greenColor') color = 'red';

    constructor(private el:ElementRef){
        el.nativeElement.addEventListener('click', this.changeColor);
    }

    changeColor = ()=>{
        this.el.nativeElement.style.backgroundColor=this.color;
    }

}